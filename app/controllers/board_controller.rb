class BoardController < ApplicationController
  def show
    @board = Board.default_board
  end
end
