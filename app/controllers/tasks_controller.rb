class TasksController < ApplicationController
  before_filter :set_board

  def new
    @task = Task.new
  end

  def create
    @task = Task.new
    redirect_to board_path(@board.id)
  end

  private 

  def set_board
    puts "params #{params.inspect}"
    @board = Board.find(params[:board_id])
  end

end
