class Board
  include Mongoid::Document
  field :name, type: String

  validates_presence_of :name

  def self.default_board
    Board.first || Board.create(name: "Default Name")
  end
end
