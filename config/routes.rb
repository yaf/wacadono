Wcadono::Application.routes.draw do
  root :to => 'board#show'

  resources :board do
      resources :tasks
  end
end
