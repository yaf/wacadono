require 'spec_helper'

describe TasksController do
  render_views

  describe "GET new" do
    it "should work from a board" do
      board = Factory.create(:board, id: 1)
      Board.should_receive(:find).with('1').and_return(board)
      get :new, board_id: 1
      response.should be_success
      assigns(:board).should be == board
      assigns(:task).should be_a(Task)
    end
  end

  describe "POST create" do
    it "should work from a board" do
      board = Factory(:board, id: 1)
      Board.should_receive(:find).with('1').and_return(board)
      post :create, board_id: 1, task: {title: 'test'}
      response.should be_redirect
      response.should redirect_to(board_path(board.id))
    end
  end
end
