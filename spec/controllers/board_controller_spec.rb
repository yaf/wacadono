require 'spec_helper'

describe BoardController do
  render_views

  it "should work" do
    board = Factory(:board)
    get 'show'
    response.should be_success
    assigns(:board).should be == board
  end
end
