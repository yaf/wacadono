FactoryGirl.define do
  factory :board do
    name "MyBoard"
  end
  factory :task do
    title "TestTask"
    description "A long description to tell a story about that task"
  end
end
