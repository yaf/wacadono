require 'spec_helper'

describe Task do
  it "should work" do
    Task.new.should be_a(Task)
  end

  it "should have a title" do
    Factory.build(:task, title: nil).should be_invalid
  end

  it "should have a description" do
    Factory.build(:task, description: nil).should be_invalid
  end
end
