require 'spec_helper'

describe Board do
  it "should be invalid without name" do
    Board.new.should be_invalid
  end

  it "should have a name" do
    board = Board.new(name: 'Some board')
    board.name.should be == "Some board"
  end

  describe "#default_board" do
    it "should build a default board when none exist" do
      Board.count.should == 0
      default_board = Board.default_board
      default_board.should_not be_nil
      default_board.should be_kind_of(Board)
      Board.count.should == 1
    end
  end

  describe "#tasks" do
    it "have no tasks" do
      board = Factory(:board)
      board.tasks.should be_empty
    end
    it "have many tasks" do
      board = Factory(:board)
      board.tasks.should be_empty
    end
  end
end
